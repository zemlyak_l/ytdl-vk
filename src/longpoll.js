const util = require("./util");

async function longPollWorker(response) {
  response.updates.forEach(async it => {
    if(it.type === "message_new") {
	    if(/(p|р)+(э+й+|е+й+|e+й|а|a|3+й+|3+a)+(3|з)+(о|o|0)+(p|р)+/i.test(it.object.message.text)) {
		    util.sendMessage(it.object.message.peer_id, "бебака завали ебало о своем рейзоре")
	    }
	    const link = it.object.message.text.match(RegExp("^(https:\/\/)((vm|media|cdn|www)?\.?(tiktok|twitter|discordapp|streamable|reddit)\.(com|net)|youtube\.com\/shorts\/).*$"));
	    const message = it.object.message
	    if(link) {
		    console.log(`detected link ${link} from ${it.object.message.from_id} in peer ${it.object.message.peer_id}`);
		    util.dl(link[0], message)
	    }
    }
  })
}

async function getNewLongPollServer() {
  try {
    return (await util.requireVKAPI("groups.getLongPollServer", `group_id=${util.groupID}`)).response;
  } catch(e) {
    console.log("FATAL", "Can't get long poll server: " + e.message);
  }
}

async function runLongPoll() {
  let longPoll = await getNewLongPollServer();

  while(true) {
    const response = await util.fetchJSON(
      `${longPoll.server}?act=a_check&key=${longPoll.key}&wait=25&mode=2&ts=${longPoll.ts}`
    );

    if(response.updates) {
      await longPollWorker(response);
      longPoll.ts = response.ts;
    } else if(response.failed === 2 || response.failed === 3) {
      longPoll = await getNewLongPollServer();
    }
  }
}

module.exports = {
  run: runLongPoll
};

